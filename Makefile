ifeq ($(PREFIX),)
	PREFIX:=/usr/local
endif

.PHONY: all
all: bin/dfi

bin/dfi: src/dfi.sh
	mkdir -p bin
	cp src/dfi.sh bin/dfi
	chmod a+x bin/dfi

.PHONY: clean
clean:
	rm -rf bin

.PHONY: install
install: all
	install -d $(DESTDIR)$(PREFIX)/bin
	install bin/dfi $(DESTDIR)$(PREFIX)/bin

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dfi
